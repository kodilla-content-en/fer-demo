const { exec, execSync } = require('child_process'),
	express = require('express'),
	bodyParser = require('body-parser'),
	fs = require('fs'), 
	path = require('path'),
	JsDiff = require('diff');

let app = express();
let gitState = {};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static('preview'));

const keysExist = function(obj, ...keys){
	for (var i = 0; i < keys.length; i++) {
		// console.log(keys[i], obj[keys[i]]);
		if(typeof(obj[keys[i]])=='undefined'){
			return false;
		}
	}
	return true;
};

const updateGitState = function(){
	gitState.currentBranch = execSync('git rev-parse --abbrev-ref HEAD').toString().trim();
	gitState.branches = ['master', gitState.currentBranch].filter((v, i, self) => self.indexOf(v)===i);
	gitState.localChanges = execSync('git status --porcelain').toString().trim();
};


const localChanges = function(){
	if(gitState.localChanges!='') {
		console.log('unsaved changes:', gitState.localChanges);
		return true;
	}

	return false;
};

const getMeta = function(reply){
	return {
		project: path.basename(path.dirname(__dirname)),
		currentBranch: gitState.currentBranch,
		localChanges: localChanges()
	};
};

const difference = function(str1, str2){
	let diffContent = JsDiff.diffLines(str1, str2);

	return diffContent.length!=1 || diffContent[0].added || diffContent[0].removed;
};

const forEach = function(obj, callback){
	Object.keys(obj).forEach(k => {callback(k, obj[k])});
};

const ensureDirectoryExistence = function(filePath){
	var dirname = path.dirname(filePath);
	if (!fs.existsSync(dirname)) {
		ensureDirectoryExistence(dirname);
		fs.mkdirSync(dirname);
	}
}

app.get('/userscript', function(req, res) {
	res.end(fs.readFileSync('server/repo-toop.user.js', 'utf8'));
});

app.get('/info', function(req, res) {
	let reply = {err: [], diffs: []};
	updateGitState();

	reply.meta = getMeta();
	res.end(JSON.stringify(reply));
});

app.post('/check', function(req, res) {
	let d = req.body;
	// console.log(d);
	// console.log(typeof(d));
	updateGitState();

	if(!keysExist(d, 'modid', 'subid', 'content')){
		res.end(JSON.stringify({msg: 'wrong format', body: d}));
		return;
	}

	let modulesPath = 'src/modules.json',
		titleFields = {content: 'title', task: 'task'},
		reply = {err: [], branches: {}},
		paths = {},
		branches = {};

	paths = {
		content: 'src/'+d.modid+'/'+d.modid+(d.subid ? '-'+d.subid : '')+'.html'
	};

	if(d.subid && d.task && d.task.title) {
		paths.task = 'src/'+d.modid+'/'+d.modid+'-'+d.subid+'-z.html';
	}

	gitState.branches.forEach(function(branch){
		let modules;
		// if(branch == gitState.currentBranch && !gitState.localChanges){
		// 	modules = JSON.parse(fs.readFileSync(modulesPath, 'utf8'));
		// }
		// else {
			modules = JSON.parse(execSync('git show '+branch+':'+modulesPath).toString().trim());
		// }
		let modulesFiltered = modules.filter(mod => mod.id==d.modid);
		
		if(!modulesFiltered.length) {
			reply.branches[branch] = false;
			return true;
		}

		let item = modulesFiltered[0];

		if(d.subid!==false){
			let submodulesFiltered = item.submodules.filter(sub => sub.id==d.subid);
			if(!submodulesFiltered.length) {
				reply.branches[branch] = false;
				return true;
			}
			item = submodulesFiltered[0];
		}

		reply.branches[branch] = {};

		forEach(paths, function(type, path){
			let part = {};
			part.title = item[titleFields[type]];
			// console.log(branch, type, part.title, titleFields[type]);
			// console.log(item);
			part.text = execSync('git show '+branch+':'+path).toString();

			let rbt = reply.branches[branch][type] = {};
			rbt.diffs = {};
			rbt.modified = execSync('git log '+branch+' -1 --format="%ad" --date=iso-strict -- '+path).toString().trim();

			forEach(['title', 'text'], function(j, comp){
				if(difference(d[type][comp], part[comp])){
					rbt.diffs[comp] = {
						content: part[comp]
					};
				}
				else {
					rbt.diffs[comp] = false;
				}
			});
		});
		
	});


	reply.meta = getMeta();
	res.end(JSON.stringify(reply));
});

app.post('/save', function(req, res) {
	let d = req.body;
	updateGitState();

	if(!keysExist(d, 'modid', 'subid', 'content')){
		res.end(JSON.stringify({msg: 'wrong format'}));
		return;
	}

	let modulesPath = 'src/modules.json',
		modules = JSON.parse(fs.readFileSync(modulesPath, 'utf8')),
		reply = {err: []}; 

	let modulesFiltered = modules.filter(mod => mod.id==d.modid);
	
	if(!modulesFiltered.length) {
		if(!d.subid) {
			modulesFiltered = [{
				id: d.modid, 
				title: d.content.title,
				submodules: []
			}];
			modules.push(modulesFiltered[0]);
		}
		else {
			res.end(JSON.stringify({msg: 'can\'t save submodule without saving module first'}));
			return;
		}
	}

	let item = modulesFiltered[0];

	if(d.subid!==false){
		let submodulesFiltered = item.submodules.filter(sub => sub.id==d.subid);
		if(!submodulesFiltered.length) {
			submodulesFiltered = [{
				id: d.subid
			}];
			item.submodules.push(submodulesFiltered[0]);
		}
		item = submodulesFiltered[0];
	}

	if(localChanges()){
		reply.err.push('writting not possible');
	}
	else {
		let path;

		if(!d.subid) {
			path = 'src/'+d.modid+'/'+d.modid+'.html';
		}
		else {
			path = 'src/'+d.modid+'/'+d.modid+'-'+d.subid+'.html';
		}

		ensureDirectoryExistence(path);
		fs.writeFileSync(path, d.content.text);
		execSync('git add '+path);
		console.log('wrote file', path);
		item.title = d.content.title;

		if(d.subid && d.task && d.task.title) {
			let taskPath = 'src/'+d.modid+'/'+d.modid+'-'+d.subid+'-z.html';
			ensureDirectoryExistence(taskPath);
			fs.writeFileSync(taskPath, d.task.text);
			execSync('git add '+taskPath);
			console.log('wrote file', taskPath);
			item.taskTitle = d.task.title;
		}

		ensureDirectoryExistence(modulesPath);
		fs.writeFileSync(modulesPath, JSON.stringify(modules, null, "\t"));

		console.log('updated file', modulesPath);

		execSync('git add '+modulesPath);
		execSync('git commit -m "Auto-update '+d.modid+(d.subid ? '.'+d.subid : '')+'"');
	}

	reply.meta = getMeta();
	res.end(JSON.stringify(reply));
});

app.listen(8000);

console.log('Express is running on port 8000');