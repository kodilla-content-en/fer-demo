// ==UserScript==
// @name         Kodilla content editing tool
// @namespace    https://kodilla.com/
// @version      0.1
// @description  internal plugin
// @author       kodilla.com
// @require      http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js
// @match        https://kodilla.com/pl/admin/bootcamp-module/*
// @match        https://kodilla.com/pl/admin/bootcamp-submodule/*
// @grant        GM_xmlhttpRequest
// ==/UserScript==

(function() {
    'use strict';

    var $ = unsafeWindow.jQuery;

    var zeroPad = function(n, width, z) {
        width = width || 2;
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };

    var trimId = function(str){
        str = str.trim().replace(/^([0-9]+)\. .*/, '$1');
        return str.length ? zeroPad(str) : str;
    };

    var $sidebar = $('#detach-bootcamp-module, #detach-bootcamp-submodule').parent();

    var toolHtml = '';

    toolHtml += '<div id="repo-tool" style="border-top: 1px solid black">';
        toolHtml += '<button id="repo-refresh">Refresh info</button>';
        toolHtml += '<div id="repo-info">';
            toolHtml += '<div id="repo-meta">';
            toolHtml += '</div>';
        toolHtml += '</div>';
        toolHtml += '<div id="repo-buttons"></div>';
        toolHtml += '<div id="repo-output"></div>';
    toolHtml += '</div>';

    $sidebar.append(toolHtml);

    var data = {
        modid: trimId($('.bootcamp-module.active').text()),
        subid: trimId($('.bootcamp-submodule.active').text()) || false,
        content: {
            title: $('.course.js-ajax-form [name="name"]').val(),
            text: $('.course.js-ajax-form').find('[name="challenges"], [name="description"]').first().val()
        },
    };

    if(data.subid && $('.course.js-ajax-form [name="task_title"]').length){
        data.task = {
            title: $('.course.js-ajax-form [name="task_title"]').val(),
            text: $('.course.js-ajax-form [name="task"]').val()
        }

    }

    console.log(data);

    var bootcamp = $('option[value^="https://kodilla.com/pl/admin/bootcamp/"]:selected').text();

    var displayMeta = function(response){
        var tmp = '';

        if(response.meta && response.branches){
            if(
                (response.meta.project == 'bc-wd' && bootcamp != 'Web Developer')
                ||
                (response.meta.project == 'bc-java' && bootcamp != 'Java Developer')
            ){
                $('#repo-meta').html('<p>Project '+response.meta.project+' is not compatible with bootcamp '+bootcamp+'</p>');
                return;
            }

            tmp += '<ul>';
                tmp += '<li>';
                    tmp += 'Module ID: '+data.modid;
                tmp += '</li>';
                tmp += '<li>';
                    tmp += 'Submodule ID: '+data.subid;
                tmp += '</li>';
                tmp += '<li>';
                    tmp += 'Current branch: '+response.meta.currentBranch;
                tmp += '</li>';
                tmp += '<li>';
                    tmp += 'Uncommited changes: '+(response.meta.localChanges ? '<strong style="color: red;">YES</strong>' : 'no');
                tmp += '</li>';
            tmp += '</ul>';

            tmp += '<h2>Changes</h2>';

            tmp += '<style>';
            tmp += '#repo-info td {border: 1px solid black; padding: 2px 3px;}';
            tmp += '</style>';

            var rows = {Branch: []};

            $.each(response.branches, function(branch, parts){
                if(parts === false) {
                    rows.Branch.push('<td rowspan="9" style="vertical-align:top">'+branch+'<br><br><strong>file not exists</strong></td>');
                    return true;
                }

                rows.Branch.push('<td>'+branch+'</td>');

                $.each(parts, function(type, part){
                    var cell = '';

                    cell += '<td>';

                    $.each(['title', 'text'], function(i, comp){
                        cell += '<div>';
                        cell += comp+' ';
                        cell += part.diffs[comp] ? '<strong>diff</strong>' : 'same';
                        cell += '</div>';
                    });

                    cell += '<div>';
                    var d = new Date(part.modified);
                    cell += d.getFullYear()+'.'+zeroPad(d.getMonth()+1)+'.'+zeroPad(d.getDate())+ " " + zeroPad(d.getHours()) + ":" + zeroPad(d.getMinutes());
                    cell += '</div>';

                    cell += '</td>';

                    if(!rows[type]) rows[type] = [];
                    rows[type].push(cell);
                });

            });

            tmp += '<table style="border: 1px; cell-padding: 3px;">';
            tmp += '<tbody>';
            $.each(rows, function(label, row){
                tmp += '<tr>';
                tmp += '<td>'+label+'</td>';
                tmp += row.join('');
                tmp += '</tr>';
            });
            tmp += '</tbody>';
            tmp += '</table>';

            if(!$('option[value^="https://kodilla.com/pl/admin/bootcamp-edition/"]:last-child').prop('selected')){
            	tmp += '<div><strong>WARNING! You are NOT editing the newest edition!</strong></div>';
            }
        }
        else {
            $('#repo-meta').html(response.msg);
            return;
        }

        $('#repo-meta').html(tmp);

        if(!response.meta.localChanges){
            $('#repo-buttons').append('<button id="repo-reset">Reset to initial</button>');
            $('#repo-buttons').append('<button id="repo-paste">Paste from '+response.meta.currentBranch+'</button>');

            $('#repo-reset').on('click', function(e){
                e.preventDefault();
            	$('.course.js-ajax-form [name="name"]').val(data.content.title);
            	$('.course.js-ajax-form').find('[name="challenges"], [name="description"]').first().val(data.content.text);
            	var descEditor = ace.edit($('#challenges, #description').first().attr('id'));
            	descEditor.setValue(data.content.text);
	            if(data.subid && data.task && $('.course.js-ajax-form [name="task_title"]').length){
	            	$('.course.js-ajax-form [name="task_title"]').val(data.task.title);
	            	$('.course.js-ajax-form [name="task"]').val(data.task.text);
                	var descEditor = ace.edit('task');
                	descEditor.setValue(data.task.text);
	            }

            });

            $('#repo-paste').on('click', function(e){
                e.preventDefault();
                var branch = response.branches[response.meta.currentBranch];

                if(branch.content.diffs.title) {
                	$('.course.js-ajax-form [name="name"]').val(branch.content.diffs.title.content);
                }
                if(branch.content.diffs.text) {
                	$('.course.js-ajax-form').find('[name="challenges"], [name="description"]').first().val(branch.content.diffs.text.content);
                	var descEditor = ace.edit($('#challenges, #description').first().attr('id'));
                	descEditor.setValue(branch.content.diffs.text.content);
                }
	            
	            if(data.subid && branch.task && $('.course.js-ajax-form [name="task_title"]').length){
		            if(branch.task.diffs.title) {
		            	$('.course.js-ajax-form [name="task_title"]').val(branch.task.diffs.title.content);
		            }
		            if(branch.task.diffs.text) {
		            	$('.course.js-ajax-form [name="task"]').val(branch.task.diffs.text.content);
	                	var descEditor = ace.edit('task');
	                	descEditor.setValue(branch.task.diffs.text.content);
		            }
	            }
            });
        }

        if(
            !response.meta.localChanges
            &&
            response.meta.currentBranch == 'master'
            &&
            (
                window.location.hash == '#save-all'
                ||
                (
                    (data.modid == 1 || data.modid === '00')
                    &&
                    data.subid === false
                )
            )
        ){
            $('#repo-buttons').append('<button id="save-all">Save all</button>');

            $('#save-all').on('click', function(e){
                e.preventDefault();
                saveAndGo();
            });

            var saveAndGo = function(){
                var active = $('.bootcamp-submodule.active');
                if(!active.length) active = $('.bootcamp-module.active');
                if(!active.length) {
                    window.location.hash = '';
                    $('#repo-output').html('no active link found');
                    return;
                }

                saveToFile();

                var next = active.next();
                if(!next.length) next = active.parent('.bootcamp-submodule-list').next();
                if(!next.length) {
                    window.location.hash = '';
                    $('#repo-output').html('no next link found - all saved');
                    return;
                }

                if(next.is('ul')) next = next.children('li').first();

                var link = next.children('a[href^="https://kodilla.com/pl/admin/bootcamp-"]').first();
                if(!link.length) {
                    window.location.hash = '';
                    $('#repo-output').html('error finding link');
                    return;
                }

                window.location.href = link.attr('href')+'#save-all';

            };

            if(window.location.hash == '#save-all') saveAndGo();
        }

    };

    var saveToFile = function(){
        GM_xmlhttpRequest({
            url: 'http://localhost:3000/save',
            method: 'POST',
            headers: { 'Content-Type': 'application/json; charset=UTF-8' },
            data: JSON.stringify(data),
            responseType: 'json',
            onerror: function(){
                try{console.error('AJAX ERROR', arguments);}
                catch(e){}
            },
            onload: function(reply){
                console.log(reply.response);
                $('#repo-output').html('saved to file');
            }

        });
    };

    var refreshInfo = function(){
        $('#repo-output').html('wait...');


        GM_xmlhttpRequest({
            url: 'http://localhost:3000/check',
            method: 'POST',
            headers: { 'Content-Type': 'application/json; charset=UTF-8' },
            data: JSON.stringify(data),
            responseType: 'json',
            onerror: function(){
                try{console.error('AJAX ERROR', arguments);}
                catch(e){}
            },
            onload: function(reply){
                console.log(reply.response);
                displayMeta(reply.response);
                $('#repo-output').html('WORKS!');
            }

        });

    };

    $('#repo-refresh').on('click', function(e){
        e.preventDefault();
        refreshInfo();
    });

    refreshInfo();

})();
