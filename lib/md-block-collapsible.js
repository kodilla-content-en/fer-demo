const shortid = require('shortid');

module.exports = {
  render: function (tokens, idx) {
    let m = tokens[idx].info.trim().match(/^collapsible\s+(.*)$/);

    if (tokens[idx].nesting === 1) {
      let id = 'collapse-'+shortid.generate()+'-toggle';
      let buttonLabel = 'ukrytą sekcję';
      if(m && m.length>1) buttonLabel = m[1];
      // opening tag
      return  '<p class="text-center">\n'+
              '  <span class="collapse-toggles">\n'+
              '    <a href="#'+id+'" class="btn btn-primary collapsed" data-toggle="collapse">Pokaż '+buttonLabel+'</a>\n'+
              '    <a href="#'+id+'" class="btn btn-primary collapsed" data-toggle="collapse">Ukryj '+buttonLabel+'</a>\n'+
              '  </span>\n'+
              '</p>\n'+
              '<div id="'+id+'" class="collapse">\n';
    } else {
      // closing tag
      return '</div>\n';
    }
  }
};