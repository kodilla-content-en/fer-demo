const { exec } = require('child_process'),
	fs = require('fs'),
  path = require('path'),
  mdLinks = require('markdown-it-link-attributes'),
  Handlebars = require('handlebars');

const indexTpl = Handlebars.compile(fs.readFileSync('src/template/index.html', 'utf8'));
const moduleTpl = Handlebars.compile(fs.readFileSync('src/template/module.html', 'utf8'));


/* Markdown-it: 
 * enable html in markdown
 * disable code blocks by indentation
 */

global.md = require('markdown-it')({
  html: true
}).disable('code');

/* add target="_blank" to all links */

md.use(mdLinks, {attrs: {target: '_blank'}});

/* use markup as default code highlighting language if none specified */

md.use(require('./md-default-code-lang'));

/* add custom snippet blocks */

const mdContainer = require('./md-container');
md.use(mdContainer, 'modal', require('./md-block-modal'));
md.use(mdContainer, 'todo', require('./md-block-todo'));
md.use(mdContainer, 'comment', require('./md-block-comment'));
md.use(mdContainer, 'box', require('./md-block-box'));
md.use(mdContainer, 'collapsible', require('./md-block-collapsible'));
md.use(mdContainer, 'codepen', require('./md-block-codepen'));

const getFileContents = require('./get-file-contents');

fs.readFile('src/modules.json', 'utf8', (err, content) => {
	let modules = JSON.parse(content);

	let indexHtml = indexTpl(modules);

	fs.writeFileSync('preview/index.html', indexHtml); 
	// console.log('Created preview/index.html');


	for (let i=0; i < modules.length; i++) {
		let mod = modules[i];
		
		mod.content = getFileContents(mod.id+'/'+mod.id);

		for (let k = 0; k < mod.submodules.length; k++) {
			let submod = mod.submodules[k];
			try {
				submod.content = getFileContents(mod.id+'/'+mod.id+'-'+submod.id);
				submod.taskContent = '';
				if(submod.task && submod.task.length) submod.taskContent = getFileContents(mod.id+'/'+mod.id+'-'+submod.id+'-z');
			} catch (e) {}
		}

		let modHtml = moduleTpl(mod);

		let pattern = /<code[^>]*>(.*?)<\/code>/g;
		let replacements = [];
		let match;

		while(match = pattern.exec(modHtml)){
			if(/[<>]/.test(match[1])){
				replacements.push({
					from: match[0], 
					to: match[0].replace(match[1], match[1].replace(/</g, '&lt;').replace(/>/g, '&gt;'))
				});
			}
		}

		for (let j = 0; j < replacements.length; j++) {
			modHtml = modHtml.replace(replacements[j].from, replacements[j].to);
		}
		
		modHtml = modHtml.replace(/"\/(static\/[^\"]*)"/g, '"https://kodilla.com/$1"');

		// let matched = modHtml.match();
		
		// matched.map(match => {modHtml.replace()});

		fs.writeFileSync('preview/'+mod.id+'.html', modHtml); 

		// console.log('Created preview/'+mod.id+'.html');
	}

	// console.log(modules);
});
