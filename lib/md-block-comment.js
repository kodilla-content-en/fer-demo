module.exports = {
  render: function (tokens, idx) {
    if (tokens[idx].nesting === 1) {
      // opening tag
      return '<div class="dev-comment" style="display: none; background: #ffffcc;">\n';
    }
    else {
      return '</div><br />';
    }
  }
};