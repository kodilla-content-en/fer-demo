const hasValue = function(value){
  return value && value != '0' && value != '-' ? value : false;
};

module.exports = {
  singleLine: true,

  render: function (tokens, idx) {
    let m = tokens[idx].info.trim().match(/^codepen\s+(.*?)(\s+\:{3})*$/);
    if(!m || m.length<2) return '';

    let params = m[1].split(' ');
    if(!params || params.length<1) return '';

    let id = params[0];
    let height = hasValue(params[1]) || '600';
    let tabs = hasValue(params[2]) || 'js';
    let user = hasValue(params[3]) || 'kodilla';
    let theme = hasValue(params[4]) || '33276';
    let clickToLoad = hasValue(params[5]) || 0;

    if(!/result/.test(tabs)) tabs += ',result';

    if (tokens[idx].nesting === 1) {
      // opening tag
      return '<p '+
                'style="min-height:'+height+'px; border: 1px solid #d0e9c6; border-radius: 4px; padding: 20px; text-align: center;" '+
                'data-height="'+height+'" '+
                'data-theme-id="'+theme+'" '+
                'data-slug-hash="'+id+'" '+
                'data-default-tab="'+tabs+'" '+
                'data-user="'+user+'" '+
                'data-embed-version="2" '+
                'data-editable="true" '+
                (clickToLoad ? 'data-preview="true" ' : '')+
                'class="codepen"'+
              '>'+
              'See <a href="https://codepen.io/'+user+'/pen/'+id+'/">the Pen</a> '+
              'on <a href="https://codepen.io">CodePen</a>.'+
              '</p><br>\n';
    }
    else {
      return '';
    }
  }
};
