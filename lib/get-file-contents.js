const Entities = require('html-entities').XmlEntities;

const entities = new Entities();

const fs = require('fs'),
	path = require('path'), 
	mkdirp = require('mkdirp');

module.exports = function(filepath){
  try {
    let html = md.render(fs.readFileSync('src/'+filepath+'.md', 'utf8'));
    mkdirp(path.dirname('preview/md-generated-parts/'+filepath+'.html'), function (err) {
      if (err) console.error(err)
      else {
      	// console.log('writing');
        fs.writeFileSync('preview/md-generated-parts/'+filepath+'.html', entities.decode(html));
      }
    });
    return html;
	} catch(e) {
		try {
			// console.log('found '+filepath+'.html', fs.readFileSync('src/'+filepath+'.html', 'utf8').substring(0,10));
			return fs.readFileSync('src/'+filepath+'.html', 'utf8');
		} catch(e) {
			// console.log('not found', 'src/'+filepath+'');
			return '';
		}
	}
};