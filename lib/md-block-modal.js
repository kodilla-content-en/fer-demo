module.exports = {
  singleLine: true, 

  validate: function(params) {
    return params.trim().match(/^modal\s+(.*)$/);
  },

  render: function (tokens, idx) {
    let m = tokens[idx].info.trim().match(/^modal\s+(\S*)(\s+(.*)\s*)?\s:::$/);
    let url = '/static/bootcamp/image.jpg';
    let maxWidth = ' max-width: 50%;';

    if(m && m.length>1) url = m[1];
    if(m && m.length>3 && ['fullwidth', 'full', '100%'].indexOf(m[3])>-1) maxWidth = '';

    if (tokens[idx].nesting === 1) {
      // opening tag
      return '<a href="'+url+'" class="img-modal-group"><img src="'+url+'" alt="image" style="width: auto;'+maxWidth+'"></a>\n';
    }
    else {
      return '';
    }
  }
};