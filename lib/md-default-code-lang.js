module.exports = function(md){
  md.renderer.rules.fence = (function() {
    var original = md.renderer.rules.fence;
    return function(tokens, idx) {
      if(!tokens[idx].info) tokens[idx].info = 'markup';
      return original.apply(this, arguments);
    };
  })();
};