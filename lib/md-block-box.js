module.exports = {

  titleLines: 1, 

  validate: function(params) {
    return params.trim().match(/^box\s+(.*)$/);
  },

  render: function (tokens, idx) {
    let m = tokens[idx].info.trim().match(/^box\s+(.*)$/);

    let dict = {
      'green': 'success',
      'yellow': 'warning',
      'red': 'danger',
      'blue': 'info'
    };

    if (tokens[idx].nesting === 1) {
      let type;
      if(m && m.length>1) type = dict[m[1]] ? dict[m[1]] : m[1];
      // opening tag
      return '<div class="bc-note alert-'+type+'">\n'+
              '<h4 class="alert-heading text-center">'+md.renderInline(tokens[idx].titleContent)+'</h4>\n';
    } else {
      // closing tag
      return '</div>\n';
    }
  }
};