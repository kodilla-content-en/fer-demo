### Nagłówek rozdziału

Rozdziały są dodawane do spisu treści



#### Nagłówek podrozdziału

Podrozdziały nie są dodawane do spisy treści. 



### Formatowanie tekstu



#### Podstawy

Nowy akapit dodaje się zostawiając pustą linię. 

Formatowanie tekstu: **pogrubienie**, *kursywa*, **_pogrubienie z kursywą_**. Znaków `*` i `_` można używać zamiennie.

[Linki](https://uploads.kodilla.com/bootcamp/wdp/solo-project-1/general.png) wstawiamy w ten sposób. 



#### Listy

- lista
- wypunktowana
  - niższy poziom listy
    - itd. 

1. lista
2. numerowana
   1. niższy poziom listy
3. można mieszać rodzaje list
   - tzn. niższy poziom może być wypunktowany
     1. itd.



#### Fragmenty kodu

Można wstawiać `w linii` lub jako blok.

``` js
// koniecznie podając nazwę języka, 
//w którym jest dany snippet kodu!
```



### Nasze widgety



#### Obrazki

Wstawiamy jako modale

::: modal https://uploads.kodilla.com/bootcamp/wdp/WDP-1-1-0.png :::



#### Boxy

::: box green
Tytuł boxa

Treść boxa
:::

::: box blue
Tytuł boxa

Treść boxa
:::

::: box yellow
Tytuł boxa

Treść boxa
:::

::: box red
Tytuł boxa

Treść boxa
:::



#### Collapsible

::::: collapsible tekst na guziku
Treść widoczna tylko po kliknięciu guzika
:::::

Kod który załączamy *tylko* w treści głównej modułu, niezależnie od tego w którym submodule używamy tego widgetu.

``` html
<style>
.bootcamp.module .module-content .card-block .collapse-toggles [data-toggle="collapse"]:last-of-type.collapsed {display: none;}
.bootcamp.module .module-content .card-block .collapse-toggles [data-toggle="collapse"]:first-of-type:not(.collapsed) {display: none;}
.bootcamp.module .module-content .card-block .collapse-toggles:hover [data-toggle="collapse"].btn-primary {color: #02ac46; background-color: #fff;}
</style>
```



#### Tabs

<ul class="nav nav-tabs">
	<li class="nav-item"><a href="#tab-id-1" class="nav-link active show" data-toggle="tab">Tab 1 title</a></li>
	<li class="nav-item"><a href="#tab-id-2" class="nav-link" data-toggle="tab">Tab 2 title</a></li>
	<li class="nav-item"><a href="#tab-id-3" class="nav-link" data-toggle="tab">Tab 3 title</a></li>
</ul>

<div class="tab-content">

<div id="tab-id-1" class="tab-pane card fade active show"><div class="card-block">

#### Tab 1 title

Markdown **content** of the tab.

</div></div> <!-- /.tab-pane -->
<div id="tab-id-2" class="tab-pane card fade"><div class="card-block">

#### Tab 2 title

Markdown **content** of the tab.

</div></div> <!-- /.tab-pane -->
<div id="tab-id-3" class="tab-pane card fade"><div class="card-block">

#### Tab 3 title

Markdown **content** of the tab.

</div></div> <!-- /.tab-pane -->

</div> <!-- /.tab-content -->


Kod który załączamy *tylko* w treści głównej modułu, niezależnie od tego w którym submodule używamy tego widgetu.

``` html
<style>
.bootcamp.module .module-content .card-block .tab-content > .tab-pane {border-radius: 0;}
.bootcamp.module .module-content .card-block .tab-content > .tab-pane > .card-block {padding: 0;}
.bootcamp.module .module-content .card-block .tab-content > .tab-pane.active {display: block;}
.bootcamp.module .module-content .card-block .tab-content > .tab-pane.show {opacity: 1;}
</style>
```



#### Codepeny

Z konta Kodilla, z domyślnymi ustawieniami

::: codepen fcd0d75a8c2ec736437bee9b01f556cd :::

Customowy codepen

``` plaintext
parametry kolejno: 
- wysokość
- otwarte-taby
- nazwa-konta
- theme-id
- click-to-run

czyli np:

::: codepen fcd0d75a8c2ec736437bee9b01f556cd 600 js kodilla 33276 1 :::
```



#### Edytor blokowy

<div class="blockly-widget" style="display: none;">
  <div class="output-area">
  </div>
  <xml xmlns="http://www.w3.org/1999/xhtml" class="workspace-blocks" style="display:none">
  
  </xml>
  <xml xmlns="http://www.w3.org/1999/xhtml" class="blockly-toolbox" style="display: none;"><category name="Messages" colour="#935ba5"><block type="print_message"></block><block type="clear_messages"></block><block type="console_log"></block></category><category name="Text" colour="#5ba580"><block type="text"><field name="TEXT"></field></block><block type="text_join"><mutation items="2"></mutation></block><block type="text_append"><field name="VAR" id="]^ERu1S%=:I$.dk=+!S-" variabletype="">item</field><value name="TEXT"><shadow type="text"><field name="TEXT"></field></shadow></value></block></category><category name="Variables" colour="#A65C81" custom="VARIABLE"></category><category name="Math" colour="#5b6da5"><block type="math_number"><field name="NUM">0</field></block><block type="math_arithmetic"><field name="OP">ADD</field><value name="A"><shadow type="math_number"><field name="NUM">1</field></shadow></value><value name="B"><shadow type="math_number"><field name="NUM">1</field></shadow></value></block><block type="math_round"><field name="OP">ROUND</field><value name="NUM"><shadow type="math_number"><field name="NUM">3.1</field></shadow></value></block><block type="math_random_float"></block></category><category name="Logic" colour="#5b80a5"><block type="controls_if"><mutation else="1"></mutation><value name="IF0"><block type="compare_values"><field name="compare_type">==</field></block></value></block><block type="compare_values"><field name="compare_type">==</field></block></category><category name="Input" colour="#5ba580"><block type="text_prompt_ext"><mutation type="TEXT"></mutation><field name="TYPE">TEXT</field><value name="TEXT"><shadow type="text"><field name="TEXT">abc</field></shadow></value></block></category><category name="Functions" colour="#9A5CA6" custom="PROCEDURE"></category><category name="Misc" colour="#9A5CA6"><block type="return_value"></block></category><category name="DOM" colour="#a5a55b"><block type="get_element_by_id"></block><block type="add_event_listener"><field name="eventType">click</field></block></category></xml>
</div>

Kod załączany **tylko** na końcu ostatniego submodule modułu, niezależnie od tego w którym submodule używamy tego widgetu.

``` html
<script type="template/html" id="blockly-elements">
    <div class="switches">
        <button class="show-blocks active">Blocks</button>
        <button class="show-fullscreen">Fullscreen</button>
    </div>
    <div class="editor">
        <div class="blockly-editor"></div>
    </div>
    <div class="buttons">
    </div>
</script>

<link rel="stylesheet" href="https://uploads.kodilla.com/bootcamp/blockly/css/style.css">
<script src="https://uploads.kodilla.com/bootcamp/blockly/js/blockly_compressed.js"></script>
<script src="https://uploads.kodilla.com/bootcamp/blockly/js/javascript_compressed.js"></script>
<script src="https://uploads.kodilla.com/bootcamp/blockly/js/blocks_compressed.js"></script>
<script src="https://uploads.kodilla.com/bootcamp/blockly/js/en.js"></script>
<script src="https://uploads.kodilla.com/bootcamp/blockly/js/acorn_interpreter.js"></script>
<script src="https://uploads.kodilla.com/bootcamp/blockly/js/blockly-config.js"></script>
<script src="https://uploads.kodilla.com/bootcamp/blockly/js/script.js"></script>
```