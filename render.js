const { exec } = require('child_process'),
	fs = require('fs'),
	Handlebars = require('handlebars');

const binpath = 'PATH=$(npm bin):$PATH ';

const indexTpl = Handlebars.compile(fs.readFileSync('src/template/index.html', 'utf8'));
const moduleTpl = Handlebars.compile(fs.readFileSync('src/template/module.html', 'utf8'));

fs.readFile('src/modules.json', 'utf8', (err, content) => {
	let modules = JSON.parse(content);

	let indexHtml = indexTpl(modules);

	fs.writeFileSync('preview/index.html', indexHtml); 
	console.log('Created preview/index.html');


	for (let i=0; i < modules.length; i++) {
		let mod = modules[i];
		
		try {
			mod.content = fs.readFileSync('src/'+mod.id+'/'+mod.id+'.html', 'utf8');
		} catch(e) {
			mod.content = '';
		}

		for (let k = 0; k < mod.submodules.length; k++) {
			let submod = mod.submodules[k];
			try {
				submod.content = fs.readFileSync('src/'+mod.id+'/'+mod.id+'-'+submod.id+'.html', 'utf8');
				submod.taskContent = '';
				if(submod.task && submod.task.length) submod.taskContent = fs.readFileSync('src/'+mod.id+'/'+mod.id+'-'+submod.id+'-z.html', 'utf8');
			} catch (e) {}
		}

		let modHtml = moduleTpl(mod);

		let pattern = /<code[^>]*>(.*?)<\/code>/g;
		let replacements = [];
		let match;

		while(match = pattern.exec(modHtml)){
			if(/[<>]/.test(match[1])){
				replacements.push({
					from: match[0], 
					to: match[0].replace(match[1], match[1].replace(/</g, '&lt;').replace(/>/g, '&gt;'))
				});
			}
		}

		for (let j = 0; j < replacements.length; j++) {
			modHtml = modHtml.replace(replacements[j].from, replacements[j].to);
		}
		
		modHtml = modHtml.replace(/"\/(static\/[^\"]*)"/g, '"https://kodilla.com/$1"');

		// let matched = modHtml.match();
		
		// matched.map(match => {modHtml.replace()});

		fs.writeFileSync('preview/'+mod.id+'.html', modHtml); 

		console.log('Created preview/'+mod.id+'.html');
	}

	// console.log(modules);
});
